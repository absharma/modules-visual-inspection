#!/usr/bin/env python3

import pyautogui
import signal
import argparse

cont = True
def signal_handler(signal, frame):
    print("You pressed ctrl+C to quit")
    global cont
    cont = False
    return
signal.signal(signal.SIGINT, signal_handler)      

parser=argparse.ArgumentParser()
parser.add_argument('-x','--x',type=int,help="x coordinate",default=1453)
parser.add_argument('-y','--y',type=int,help="y coordinate",default=635)
args=parser.parse_args()

pyautogui.PAUSE = 1
pyautogui.FAILSAFE = True

pyautogui.click(args.x,args.y)