#!/usr/bin/env python
#############################################
# 
# Florian.Haslbeck@cern.ch
# December 2019
# Standa motion controller class
# May need verification of conversion factor mm to steps and vice versa
# 
#############################################

import io
import serial
from time import sleep

class StandaAxis:
    def __init__(self,
    		 axis,
                 port,
                 baudrate=115200,
                 stopbits=2,
                 timeout=0.4,
                 verbose = False
                 ):
        self.com = serial.Serial(port=port,baudrate=baudrate,stopbits=stopbits,timeout=timeout)
        self.verbose = False
        self.superVerbose = False
        self.moving = False
        self.axis = axis
        self.gauge = {"x":{"min":4294175855,"max":4294579971},	#total steps: 40700
                      "y":{"min":192123,"max":594220},	#total steps: 40700  
                      "z":{"min":-2900,"max":37000}}	#total steps: 39900
        rmax = self.stepToMm(abs(self.gauge[self.axis]['max']) - self.gauge[self.axis]['min'])
        print("%s axis range 0.000 - %.3f [mm]"%(self.axis,rmax))
        pass
    
    def setVerbose(self,enable=True,superV=False):
        self.verbose = enable
        self.superVerbose = superV
        pass

    def fromString(self,reply,start,size):  #convert the controller reply (bytes encoded as chars) to value
        cnv = 0
        for b in xrange(size):
            cnv+=ord(reply[b+start])<<(b*8) #shift: little endian byte order
            pass
        return cnv

    def toString(self,val,n):
        ret=""
        for b in xrange(n):
            ret+=chr((val>>(b*8))&0xFF)
            pass
        return ret

    def write(self,cmd):#if only write() is used, flush before new cdm
        self.com.write(cmd)
        pass

    def read(self, bytesize):
        rep = self.com.read(size=bytesize)
        if len(rep) != bytesize:
            print ("%s: Error!!!!"%self.axis)
            print ("%s: got \t%i / %i Bytes"%(self.axis,len(rep),bytesize))
        return rep
       
    def writeAndRead(self, cmd, bytesize = None):
        if self.superVerbose:
            print ("%s: req: \t%s" %(self.axis,":".join("{:02x}".format(ord(c)) for c in cmd))) #req in hex
            pass
        self.write(cmd)
        rep = self.read(bytesize)
        if self.superVerbose:
            print ("%s: rep: \t%s" % (self.axis,":".join("{:02x}".format(ord(c)) for c in rep))) #rep in hex
            pass
        return rep
        
    def getStatus(self):
        if self.superVerbose: print("%s getStatus()"%self.axis)
        rep = self.writeAndRead('gets',54) 
        cmdstate = self.fromString(rep,5,1)
        if self.superVerbose: print ("%s: getStatus::state: 0x%x" % (self.axis,cmdstate))
        if cmdstate & 0x80:                 #check if the stage is still moving
            self.moving = True
            pass
        else:
            self.moving = False
            pass
        if self.verbose: print("%s getStatus() moving: %s"%(self.axis,str(self.moving)))
        pass
        
    def waitForStop(self):
        #if self.verbose: print("%s waitForStop()"%self.axis)
        while self.isMoving():
            sleep(.1)
            pass
        if self.verbose: print('%s: STOPPED'%self.axis)
        pass
        
    def isMoving(self):
        self.getStatus()
        if self.superVerbose: print("%s is moving: %s"%(self.axis,str(self.moving)))
        return self.moving

    def goHome(self):
    	#self.setPos(0,0)
        self.setPos(0)
        pass

    def moveLeft(self):
        self.writeAndRead('left',4)
        pass

    def moveRight(self):
        self.writeAndRead('rigt',4)
        pass

    def stop(self):
        self.writeAndRead('stop',4)
        pass
    
    def mmToStep(self,mm):
    	if "x" in self.axis: return int(mm * 404116/50.)
    	if "y" in self.axis: return int(mm * 402097/50.)
    
    def stepToMm(self,step):
        if "x" in self.axis: return (step*1.)/(50./404116)	
        if "y" in self.axis: return (step*1.)/(50./402097)
    
    def gaugeTo(self,pos):  	#steps to standa intrinsic pos
	    return pos + self.gauge[self.axis]['min']

    def gaugeFrom(self,pos):	#standa pos to steps
        if pos < self.gauge[self.axis]['max'] + 1:
            pos -= +self.gauge[self.axis]['min']
            pass
        else:
            pos = pos - self.gauge[self.axis]['min'] - 4294967296  #hex: 0x100000000
            pass
        return pos 

    def checkStep(self,pos): 	#check step is in axis range and int
    	rmax = abs(self.gauge[self.axis]['min']) + self.gauge[self.axis]['max']
    	valid = (type(pos)==int and pos>-1 and pos<(rmax+1))
    	if not valid: print("%s: checkStep:ERROR:\t%i step(s) outside %s axis range [0,%i] [steps]"%(self.axis,pos,self.axis,rmax))
    	return valid
    	
    def checkRange(self,mm):
    	rmax = self.stepToMm(abs(self.gauge[self.axis]['min']) + self.gauge[self.axis]['max'])
    	valid = (mm >= 0 and mm <= rmax)
    	#if not valid: print("%s: checkRange:ERROR:\t%.3f mm outside %s axis range [0.000,%.3f] [mm]"%(self.axis,mm,self.axis,rmax))
    	return valid
    	
    def setPos(self,mm):
    	if self.checkRange(mm):
    		stp = self.mmToStep(mm)
    		pos = self.gaugeTo(stp)
    		cmd="move"
    		cmd+=self.toString(pos,4)
    		cmd+=self.toString(0,8)
    		crc=self.crc16(cmd[4:])
    		cmd+=self.toString(crc,2)
    		self.writeAndRead(cmd,4)
    		pass
    	pass

    '''    
    def moveRel(self,mm):
    	newmm = float(mm + self.getPos())
    	if self.verbose: print("%s: try to move to pos: %.3f"%(self.axis,newmm))
        if self.checkRange(newmm):

    		stp = self.mmToStep(newmm)
    		pos = self.gaugeTo(stp)
                print("pos = %.3f"%pos)
                print("test")
	    	self.waitForStop()
                print("test2")
	    	print("moveRel::stopped")
                cmd = "movr"
	    	cmd+=self.toString(pos,4)
	    	cmd+=self.toString(0,8)
	    	crc=self.crc16(cmd[4:])
	    	cmd+=self.toString(crc,2)
	    	self.writeAndRead(cmd,4)
	    	pass
        else:
                print("%s: %.3f is out of %s range "%(self.axis,newmm,self.axis))
                pass
        self.waitForStop()
        return self.getPos()
    	#pass
    '''
    def moveRel(self,mm):
        np = float(self.getPos() + mm)
        self.setPos(np)
        pass

    def getPos(self):
        rep = self.writeAndRead('gpos',26)
        pos = self.fromString(rep,4,4)
        print(pos)
        stp = self.gaugeFrom(pos)
        mm = self.stepToMm(stp)
        print("mm: ",mm)
        if self.verbose: print("%s: getPos::pos [mm] \t%.3f"%(self.axis,mm))
        return mm
    
    def getSpeed(self):
        rep = self.writeAndRead('gmov',30)
        speed = self.fromString(rep,4,4)  
        if self.verbose: print("%s: speed %s"%(self.axis,str(speed)))
        return speed
	
    def setSpeed(self,speed):
        cmd='smov'
        cmd+=self.toString(speed,4)
        cmd+=self.toString(0,20)
        crc=self.crc16(cmd[4:])
        cmd+=self.toString(crc,2)
        self.writeAndRead(cmd,4)
        pass

    def getAcceleration(self):
        rep = self.writeAndRead('gmov',30)
        return self.fromString(rep,9,2)

    def setAcceleration(self,acceleration):
        cmd='smov'
        cmd+=self.toString(0,5)
        cmd+=self.toString(acceleration,2)
        cmd+=self.toString(0,17)
        crc=self.crc16(cmd[4:])
        cmd+=self.toString(crc,2)
        self.writeAndRead(cmd,4)
        pass	
		
    def getDeceleration(self):
        rep = self.writeAndRead('gmov',30)
        return self.fromString(rep,11,2)

    def setDeceleration(self,deceleration):
        cmd='smov'
        cmd+=self.toString(0,2)
        cmd+=self.toString(deceleration,2)
        cmd+=self.toString(0,15)
        crc=self.crc16(cmd[4:])
        cmd+=self.toString(crc,2)
        self.writeAndRead(cmd,4)
        pass	

    def getEncoderInfo(self):  #is NUl
        rep = self.writeAndRead('geni',70)
        man = self.fromString(rep,4,16)
        par = self.fromString(rep,20,24)
        res = self.fromString(rep,44,24)
        print("man %i"%man)
        print("par %i"%par)
        print("res %i"%res)
        pass

    def crc16(self,pbuf):		# pbuf: data array
        crc = 0xffff					
        carry_flag = 0
        a = 0
        n=len(pbuf)
        for i in range(0,n):
            crc = crc ^ ord(pbuf[i])	# bitwise XOR : x ^ y : x if y=0, complement x if y=1
            for j in range(0,8):
                a = crc
                carry_flag = a & 0x0001	# bitwise AND: 	x & y : 1 if x AND y both 1, else 0 
                crc = crc >> 1		# rightshift (crc = (crc dived by 2**1))
                if (carry_flag == 1 ):
                    crc = crc ^ 0xa001
                    pass
                pass
            pass
        return crc
       
    def checkCRC(self,rep):
        correctCRC = False
        l = len(rep)
        if l > 4:
            calCRC=self.crc16(rep[4:-2])  			#calculate crc
            recCRC=self.fromString(rep,len(rep)-2,2) 	#received crc
            correctCRC = (calCRC == recCRC)
            pass		
        elif l == 4:
            correctCRC = True
            pass
        elif l < 4:
            print("Answer shorter than 4 Bytes!!")
            pass
        return correctCRC


